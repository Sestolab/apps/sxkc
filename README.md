# sxkc

Show X Key Code.

## Dependencies

* libxcb

## Installation

* $ make
* # make install

## Examples

Run sxkc, then press any key to print its key code to standard output:

```
$ sxkc
```

Write output to the clipboard using [sxc](https://sestolab.pp.ua/apps/sxc):

```
$ sxkc | sxc
```

