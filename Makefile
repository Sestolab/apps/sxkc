CFLAGS += -Wall -Wextra -pedantic -lxcb

PREFIX ?= /usr/local
CC ?= cc

all: sxkc

sxkc: sxkc.c
	$(CC) sxkc.c $(CFLAGS) -o sxkc

install: sxkc
	install -Dm755 sxkc -t $(DESTDIR)$(PREFIX)/bin

uninstall:
	rm -f $(DESTDIR)$(PREFIX)/bin/sxkc

clean:
	rm -f sxkc

.PHONY: all install uninstall clean

