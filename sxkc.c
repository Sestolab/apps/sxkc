#include <stdio.h>
#include <stdlib.h>
#include <xcb/xcb.h>


int main(void)
{
	xcb_connection_t *c = xcb_connect(NULL, NULL);

	if (xcb_connection_has_error(c))
	{
		fprintf(stderr, "Could not connect to the X server.\n");
		xcb_disconnect(c);
		return 1;
	}

	xcb_screen_t *s = xcb_setup_roots_iterator(xcb_get_setup(c)).data;

	xcb_grab_keyboard_reply_t *grab_keyboard = xcb_grab_keyboard_reply(c, xcb_grab_keyboard(c, 1, s->root,
		XCB_CURRENT_TIME, XCB_GRAB_MODE_ASYNC, XCB_GRAB_MODE_ASYNC), NULL);

	if (grab_keyboard->status != XCB_GRAB_STATUS_SUCCESS)
	{
		fprintf(stderr, "Failed to grab keyboard.\n");
		free(grab_keyboard);
		xcb_disconnect(c);
		return 1;
	}
	free(grab_keyboard);

	xcb_generic_event_t *e = NULL;
	while ((e = xcb_wait_for_event(c)))
	{
		if ((e->response_type & ~0x80) == XCB_KEY_PRESS)
		{
			printf("%d", ((xcb_key_press_event_t *)e)->detail);
			break;
		}
	}
	free(e);

	xcb_ungrab_keyboard(c, XCB_CURRENT_TIME);
	xcb_disconnect(c);
	return 0;
}

